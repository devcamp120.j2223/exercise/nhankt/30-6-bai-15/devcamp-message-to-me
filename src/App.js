import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react';
import InputMess from './Components/InputMess';
import OutputMess from './Components/OutputMess';
import Title from './Components/Title';


function App() {
  const [output,setOutput]= useState([])
  const handleInput = (paramInput)=>{
    if (paramInput!="" && paramInput!= undefined) {
    setOutput([...output,paramInput])}
  }
  return (
    <div className='text-center'>
      <Title></Title>
      <InputMess inputMessPrint={handleInput} lastMess={output[output.length-1]}></InputMess>
      <OutputMess outputMessDisplay={output}></OutputMess>
    </div>
  );
}

export default App;
