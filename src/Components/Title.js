import image from "../images/background.jpg"
export default function Title() {
    return (
        <>
            <div className="row mt-5">
                <div className="col-12">
                    <h1>Chào mừng đến với Devcamp120</h1>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-12">
                    <img src={image} alt="background" width={500} />
                </div>
            </div>
        </>
    )
}