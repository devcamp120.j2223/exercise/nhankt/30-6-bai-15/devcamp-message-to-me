import { useState } from "react"
import { Col, Container, Row,Input, Button, Label } from "reactstrap"

export default function InputMess({inputMessPrint,lastMess}){
    let mess = ""
    let checkChange= false
    const handleChangeInput = (event) =>{
        mess=event.target.value
        checkChange= true
    }
    
    const handleBtnClick = ()=>{
        if (checkChange == false && mess =="") {mess =lastMess}
        inputMessPrint(mess)
    }
    return (
        <Container>
            <Row>
                <Col>
                    <Label >Message cho bạn 12 tháng tới:</Label>
                </Col>
            </Row>
            <Row>
                <Col xs={6} className="mx-auto mb-2">
                    <Input placeholder="Nhập message" onChange={(event)=>handleChangeInput(event)} ></Input>
                </Col>
            </Row>
            <Row >
                <Col>
                    <Button color="success" onClick={handleBtnClick}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </Container>
    )}
