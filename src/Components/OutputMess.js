import { Col, Container, Input, Label, Row } from "reactstrap"
import image from "../images/like.png"
export default function OutputMess ({outputMessDisplay}){
    return (
            <Container>
                <Row className="row mt-3">
                    <Col className="col-12"> 
                    {outputMessDisplay.map((el,index) => (<div key={index}>{el}</div>))}
                    </Col>
                </Row>
                <div>
                    {outputMessDisplay["0"]!==undefined?<img src={image} width="100px"></img>:<div></div>}
                </div>
            </Container>  
    )
}